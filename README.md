# Ruche2021

Nouvelle version du code Ruche

Dossiers

## TTGO-Ruche
Contient le code du transmetteur LoRa

## TTGO-Passerrelle
Contient le code du récepteur LoRa.
Retransmet les données à :
* Thingspeak en HTTPS
* Au serveur Raspi en HTTP

## Raspi-Serveur
Contient le code du serveur HTTP

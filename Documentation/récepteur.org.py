########################################################################
# Notes
########################################################################
"""
Nécessite logiciel de bas niveau : 
Pycom MicroPython 1.20.2.r1 [v1.11-a5aa0b8] on 2020-09-09
"""

########################################################################
# Modules
########################################################################
 
from network import WLAN
from network import LoRa
from network import Server

import machine
from machine import RTC
from machine import Timer
from machine import WDT
from machine import SD

import _thread
import gc as garbageCollector
import os
import pycom
import re
import socket
import time
from time import sleep,sleep_ms
from ustruct import unpack

########################################################################
# PARAMÈTRES DEBOGGAGE
########################################################################
DEBUG_MEMORY = False
DEBUG_IP = False

########################################################################
# Gestion LED
########################################################################

pycom.heartbeat(False)
def blickLED():
    pycom.rgbled(0xf00000)
    sleep_ms(500)
    pycom.rgbled(0x00f000)
    sleep_ms(500)
    pycom.rgbled(0x00000f)
    sleep_ms(500)
    pycom.rgbled(0x000000)

########################################################################
# Réseau WiFi
########################################################################

class WiFi_Handler():

    wlan_Interface = None
    
    
    def __init__(self):
        self.__alarm = Timer.Alarm(self._handler, 10,
                                   periodic=True)

        # Param Claude
        #self.ssid = "NETGEAR"
        #self.password = "jaggedbreeze2"
        
        #Param Ici
        self.ssid = "Bbox-A210A0"
        self.password = "991D2C134E"
        self._blink = None
        self._ledstate = False
        
        self._startWiFi_mode_STA() # (mode = WLAN.STA)
        print("WiFi started")
        print(self.getAddress())


    def _wifiConfigBlink_handler(self):
        if self._ledstate:
            self._ledstate = False
            pycom.rgbled(0x000000)
        else:
            self._ledstate = True
            pycom.rgbled(0x0000ff)
       
    def _startWiFi_mode_AP(self):
        print("Start WiFi AP Mode")
        try:
            self.wlan_Interface.disconnect()
        except:
            pass

        self.wlan_Interface = WLAN(mode=WLAN.AP, ssid="LoPy")

        self._blink = Timer.Alarm(self._wifiConfigBlink_handler, 1,
                                  periodic=True)
        
    def _startWiFi_mode_STA(self):
        print("Start WiFi STA Mode")
        try:
            self.wlan_Interface.disconnect()
        except :
            pass
        
        self.wlan_Interface = WLAN(mode=WLAN.STA)
        self.wlan_Interface.connect(ssid=self.ssid,
                                    auth=(WLAN.WPA2, self.password))

        try:
            while self.wlan_Interface.ifconfig()[0]=="0.0.0.0":
                if DEBUG_IP:
                    print("-IP->",self.getAddress())
        except KeyboardInterrupt:
            self.ssid=input("SSID:")
            self.password=input("MDP:")
            self._startWifi_mode_STA()
            
        try:
            del(self._blink)
        except:
            pass
        
    def _startWiFi(self, mode):
        print("Start WiFi")
        if mode==WLAN.STA_AP:
            self._startWiFi_mode_AP()
        if mode==WLAN.STA:
            self._startWiFi_mode_STA()
        else:
            self._startWiFi_mode_STA()
            
    def _handler(self, alarm):
        if (self.wlan_Interface.ifconfig()[0]=="0.0.0.0"):
            print("WiFi reset")
            self._startWiFi(mode = WLAN.STA)
            try:
                webserver.serversocket.bind((WiFiManager.getAddress(), 80))
            except:
                print("Socket binding error")
            print("ALLOC:",garbageCollector.mem_alloc())
            print("FREE:",garbageCollector.mem_free())
            garbageCollector.collect()
            print("ALLOC:",garbageCollector.mem_alloc())
            print("FREE:",garbageCollector.mem_free())

    
        if DEBUG_IP:
            print(self.wlan_Interface.isconnected(),
                  "--",
                  self.wlan_Interface.ifconfig())

    def setSSID(ssid):
        self.ssid = ssid

    def setPWD(pwd):
        self.password = pwd
    
    def getAddress(self):
        interface = self.wlan_Interface
        return interface.ifconfig()[0]+"!"

########################################################################
# Horloge
########################################################################

class clock(RTC):
    def __init__(self):
        self.internalClock = RTC()
        self.internalClock.ntp_sync("fr.pool.ntp.org",update_period=3600)
        while not self.internalClock.synced():
            pass
        time.timezone(7200)
        print("Clock initialized")

    def now(self):
        return time.localtime()

########################################################################
# Données/Données temporelles
########################################################################
class dataSet(list):
    def __init__(self):
        self.data = []

    def _validate(self, datum):
        pass

    def add(self, datum):
        pass

########################################################################
# Traitement Trame
########################################################################
def isFrameValid(frame):
    if len(frame)!=15:
        return False

    datumAsList = unpack(">BBBBBBBLBBBB", frame[0:15])

    sep1 = datumAsList[0]
    sep2 = datumAsList[8]
    sep3 = datumAsList[10]
    sep4 = datumAsList[11]
    if ((sep1 == 0) and (sep2 == 0) and (sep3 ==0) and (sep4 == 0)):
        print("Frame OK")
        return True
    else:
        return False
    
def frame2data(frame):        
    datumAsList = unpack(">BBBBBBBLBBBB", frame[0:15])
    
    iddev = datumAsList[1]
    extpress = datumAsList[2]
    exttemp = datumAsList[3]
    exthum = datumAsList[4]
    inttemp1 = datumAsList[5]
    inttemp2 = datumAsList[6]
    weight = datumAsList[7]
    length = datumAsList[9]
    
    return {"id":iddev,
            "extpress":extpress,
            "exttemp":exttemp,
            "exthum":exthum,
            "inttemp1":inttemp1,
            "inttemp2":inttemp2,
            "weight":weight,
    }

def meanHour(data):
    meanOfData = {"id":0,
                      "express":0,
                      "extemp":0,
                      "exthum":0,
                      "inttemp1":0,
                      "inttemp2":0,
                      "weight":0,
    }
    if len(data)==1:
        for x in meanOfData:
            meanOfData[x] = data [x]
        meanOfData["date"] = data["date"]
        meanOfData["time"] = data["time"].split(":")[0] 
    else:
        
            for i in self.dataInHour:
                for idx in meanOfData:
                    meanOfdata[idx] += self.dataInHour[i][idx]
                    for idx in meanOfData:
                        meanOfdata[idx]/=len(self.dataInHour)
                        meanOfData['date']=self.dataInHour[0]['date']
                        meanOfData['time']=self.dataInHour[0]['time'].split(":")[0]
                        # COMPUTE MEAN OF dataInHour dataInDayStorage
                        self.dataInHour = [datum]
                        self.dataInDay.append(meanOfData)

        return meanOfHour
    
                        if ((self.lastFrameDay != time[2])):
                            # COMPUTE MEAN OF dataInDay
                            # STORE IN SD
                            #            
                            self.dataInHour = [datum]
                    print("Compute Mean of Day")
                    meanOfData = {"id":0,"express":0,"extemp":0,"exthum":0,
                                  "inttemp1":0,"inttemp2":0,"weight":0} 
                    for i in self.dataInDay:
                        for idx in meanOfData:
                            meanOfdata[idx]+=self.dataInDay[i][idx]
                    for idx in meanOfData:
                        meanOfdata[idx]/=len(self.dataInDay)
                        meanOfData['date']=self.dataInHour[0]['date']
                        # COMPUTE MEAN OF dataInHour dataInDayStorage
                        
                    self.dataInHour = [datum]
                    self.dataInDay = []

########################################################################
# Récepteur LoRa
########################################################################

class appLoRaIf(LoRa):

    def __init__(self):
        print("Init LoRa Object")
        self.interface = LoRa(LoRa.LORA, region=LoRa.EU868,
                      frequency = 868100000,bandwidth=LoRa.BW_125KHZ, 
                      sf=7, coding_rate=LoRa.CODING_4_5,
                      preamble=8,  power_mode=LoRa.ALWAYS_ON, 
                      public=False)

        self.interface.callback(trigger=LoRa.RX_PACKET_EVENT,
                        handler=self._onReceive)

        self.loraSocket = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
        self.loraSocket.settimeout(10)
        self.loraSocket.setblocking(False)
        print("LoRa Object OK")
        self.dataInHour = dataSet()
        self.dataInDay = dataSet()
        self.lastFrameTime = (0,0,0,0,0)

    # Format de la Trame
    #
    # OCTET > 0   | 1   | 2        | 3       | 4      | 5        | 6        | 7...10 | 11 | 12     | 13 | 14 |
    # DATA  > 0   | ID  | EXTPRESS | EXTTEMP | EXTHUM | INTTEMP1 | INTTEMP2 | WEIGHT | 0  | LENGTH | 0  | 0  |
    # STRUCT> B     B     B          B         B        B          B          L        B    B        B   B
    # VAR   > sep1|iddev| extpress | exttemp | exthum | inttemp1 | inttemp2 | weight |sep2| length |sep3|sep4| 
    #

    
    def _dataStore(self, frame, time):
        datumAsList = unpack(">BBBBBBBLBBBB", frame[0:15])

        iddev = datumAsList[1]
        extpress = datumAsList[2]+960
        exttemp = datumAsList[3]-128
        exthum = datumAsList[4]
        inttemp1 = datumAsList[5]-128
        inttemp2 = datumAsList[6]-128
        weight = int((datumAsList[7]-250000)/17000)

        print("From:",iddev)
        print("EXT PRESS:",extpress)
        print("EXT TEMP:",exttemp)
        print("ext hum:",exthum)
        print("EXT INT TEMP1:",inttemp1)
        print("EXT INT TEMP2:",inttemp2)
        print("WEIGHT:",weight)
            
        
        for i in self.dataInHour:
            print("->",i)
        print("-"*10)

                    
                    
        self.lastFrameYear = time[0]
        self.lastFrameMonth = time[1]
        self.lastFrameDay = time[2]
        self.lastFrameHour = time[3]
        self.lastFrameMin = time[4]
        self.dataInHour.append(datum)
        print("Data added")
        print(self.dataInHour)

    def _onReceive(self,interface):
        print("LoRa Event",interface.events())
        timeOfReception = appClock.now()
        receivedData = self.loraSocket.recv(15)
        print(timeOfReception, "=>", receivedData)
        if self.isFrameValid(receivedData):
            print("Data Validated")
            datum = frame2data(receivedData)
            print(timeOfReception, "==>", datum)

            blickLED()

            garbageCollector.collect()
            memory = garbageCollector.mem_alloc()
            
            datum["date"]=str(timeOfReception[0])+"-"+str(timeOfReception[1])+"-"+str(timeOfReception[2]),
            datum["time"]=str(timeOfReception[3])+":"+str(timeOfReception[4])
            datum["memory"] = memory
            
            if ((timeOfReception[0]==self.lastFrameTime[0]) and
                (timeOfReception[1]==self.lastFrameTime[1]) and
                (timeOfReception[2]==self.lastFrameTime[2]) and
                (timeOfReception[3]==self.lastFrameTime[3])):
                self.dataInHour.append(datum)

            if ((timeOfReception[0]==self.lastFrameTime[0]) and
                (timeOfReception[1]==self.lastFrameTime[1]) and
                (timeOfReception[2]==self.lastFrameTime[2]) and
                (timeOfReception[4]!=self.lastFrameTime[4])):
                print("Compute mean of dataInHour") 
                self.dataInDay.append(meanHour(self.dataInHour))
                self.dataInHour = [datum]
                
            if ((timeOfReception[0]==self.lastFrameTime[0]) and
                (timeOfReception[1]==self.lastFrameTime[1]) and
                (timeOfReception[2]!=self.lastFrameTime[2])!
                print("Compute mean of dataInDay")
                storage.append(meanHour(self.dataInDay), '/sd/data.json')
                self.dataInDay = []
                self.dataInHour = [datum]

                

                
            self._dataStore(receivedData, timeOfReception)
            

########################################################################
# Alarme
########################################################################

class DisplayData:

    def __init__(self):
        self.__alarm = Timer.Alarm(self._handler, 30, periodic=True)
        print("Display process initialized")
        
    def _handler(self, alarm):
        print(data)
        try:
            print(webserver.serversocket)
            print(dir(webserver.serversocket))
        except:
            pass

class memoryInfo:

    def __init__(self):
        self.__alarm = Timer.Alarm(self._handler, 1, periodic=True)
        print("Memory process initialized")
        
    def _handler(self, alarm):
        global oldMemory
        memoryBeforeGC = garbageCollector.mem_alloc()
        garbageCollector.collect()
        memoryAfterGC = garbageCollector.mem_alloc()
        if DEBUG_MEMORY:
            print("MEMORY:", memoryBeforeGC, 
		  "-->", memoryAfterGC, 
		  "/", garbageCollector.mem_free())

        watchDog.feed()

########################################################################
# Gestionnaire carte SD
########################################################################

class SDManager():
    def __init__(self):
        self._sdAvailable = False
        self._SDinterface = None
        
        try:
            self._SDinterface = SD()
            os.mount(self._SDinterface, '/sd')
            self._sdAvailable = True
        except:
            print("No SD card inserted")

    def append(self,datum,file):
        if self._sdAvailable:
            try:
                f = open(file, 'a')
                f.write(str(datum))
                f.close()
            except Exception as E:
                print("Write error:", E)
        else:
            print("No SD card inserted")



########################################################################
# Serveur Web 
########################################################################
def _index(clientsocket,r):
    page = """HTTP/1.1 200 OK\r\nContent-Type:text/html\r\nConnection:close \r\n\r\n""" #HTTP response
    page += "<html><body><h1> Data at" + str(appClock.now()) + "</h1>"
    
    tabular = "<h2>Données de la dernière heure</h2><table border='10px' >"
    if len(lora_Interface.dataInHour)>0:
	tabular += "<tr>"+"<th>Date</th><th>Heure</th><th>ID</th><th>Ext Press</th><th>Ext Temp</th><th>Ext Hum</th><th>Int Temp 1</th><th>Int Temp 2</th><th>Weight</th><th>Memory</th></tr>"
        print(lora_Interface.dataInHour)
	for i in lora_Interface.dataInHour:
	    tabular += "<tr>"
	    tabular += "<td>"+str(i["date"])+"</td>"
            tabular += "<td>"+str(i["time"])+"</td>"
            tabular += "<td>"+str(i["id"])+"</td>"
	    tabular += "<td>"+str(i["extpress"])+"</td>"
	    tabular += "<td>"+str(i["exttemp"])+"</td>"
	    tabular += "<td>"+str(i["exthum"])+"</td>"
	    tabular += "<td>"+str(i["inttemp1"])+"</td>"
	    tabular += "<td>"+str(i["inttemp2"])+"</td>"
	    tabular += "<td>"+str(i["weight"])+"</td>"
	    tabular += "<td>"+str(i["memory"])+"</td>"
	    tabular += "</tr>\n\r"
        tabular += "</table>
        

        tabular += "<h2>Données du jour courant</h2><table border='10px'>"
        for i in lora_Interface.dataInDay:
	    tabular += "<tr>"
	    tabular += "<td>"+str(i["date"])+"</td>"
            tabular += "<td>"+str(i["time"])+"-"+str(i["time"]+1)+"</td>"
            tabular += "<td>"+str(i["id"])+"</td>"
	    tabular += "<td>"+str(i["extpress"])+"</td>"
	    tabular += "<td>"+str(i["exttemp"])+"</td>"
	    tabular += "<td>"+str(i["exthum"])+"</td>"
	    tabular += "<td>"+str(i["inttemp1"])+"</td>"
	    tabular += "<td>"+str(i["inttemp2"])+"</td>"
	    tabular += "<td>"+str(i["weight"])+"</td>"
	    tabular += "<td></td>"
	    tabular += "</tr>\n\r"
            
	tabular +="</table>"
        tabular +="<h2>Données annuelles</h2><tableborder='10px'>"
        tabular +="</table>"
    else:
	tabular = "EMPTY"
            
    page += tabular
    page += "</body></html>"
            
    clientsocket.send(page)
    clientsocket.close()
        
def _param(clientsocket,r):
    page = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nConnection:close \r\n\r\n" #HTTP response
    page += """<html><body>
    <h1> Data </h1>
    <form action="/paramset" method="get">
    <label for="ssid">SSID:</label>
    <label for="pwd">PASSWORD:</label>
    <input type="text" id="ssid" name="fssid" value="your ssid"></input>
    <input type="text" id="pwd" name="fpwd" value="the password"></input>
    <input type="submit" value="Set"></input>
    </form>
    </body>
    </html>
    """


    clientsocket.send(page)
    clientsocket.close()

def _paramset(clientsocket,r):
    print("Paramètres:",r)
        
    rFirst = r.decode("utf-8").split("?")[1]
    params = rFirst.split(" ")[0].split("&")
    ssid = params[0].split("=")[1].replace("+"," ")
    pwd  = params[1].split("=")[1].replace("+"," ")
    
    print(ssid,pwd)
    
    WiFiManager.setSSID(ssid)
    WiFiManager.setPASSWORD(pwd)
    WiFiManager._startWiFi(mode=WLAN.STA)
    
    page = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nConnection:close \r\n\r\n" #HTTP response
    page += """<html><body>OK</body></html>"""

    clientsocket.send(page)
    clientsocket.close()
    

class webServer():

    def __init__(self):
	self.srv = socket.socket()
	print("Starting web server")
	self.srv = socket.socket(socket.AF_INET,
                                          socket.SOCK_STREAM)
	self.srv.setsockopt(socket.SOL_SOCKET,
                                     socket.SO_REUSEADDR, 1)

        self.srv.bind(("0.0.0.0", 80))
	self.srv.listen(5)

        self.srv.setblocking(False)
        print("Web server Created")
                    
    def _client_worker(self,clientsocket):
        makeurl = {
            '/ ':_index,
            '/param ':_param,
            '/paramset?':_paramset,
        }
        print("Client connected")
	r = clientsocket.recv(4096)
        
	# If recv() returns with 0 the other end closed the connection
	if len(r) == 0:
	    clientsocket.close()
	    return
        
        for url in makeurl:
            if "GET "+url in str(r):
                makeurl[url](clientsocket,r)
                
    def _server_core(self, srv):
        (clientsocket, address) = self.srv.accept()
        print("Web server started")
	print("Waiting for a request")
	# Accept the connection of the clients
        
	    
	_thread.start_new_thread(self._client_worker, (clientsocket,))
        
	print("loop")
        
    def run(self):
        _thread.start_new_thread(self,_server_core(),(self.srv))
        
       


########################################################################
# Objets
########################################################################

watchDog = WDT(timeout=120000)

print("Ici")
WiFiManager = WiFi_Handler()

# L'objet horloge dépend de l'objet WiFi (utilisation de NTP)
appClock=clock()

lora_Interface =appLoRaIf()

#periodic = DisplayData()

data=[]

periodic2 = memoryInfo()

storage = SDManager()

server = Server(login=("lopy", "ylop"), timeout=120)

webserver = webServer()
#webserver.run()


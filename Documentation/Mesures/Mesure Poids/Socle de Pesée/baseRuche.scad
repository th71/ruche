FOND_BASE_LARG=430 ;
FOND_BASE_LONG=450 ;
FOND_BASE_HAUT=45 ;

RAYON_PIED=5 ;

cube([FOND_BASE_LARG,FOND_BASE_LONG,FOND_BASE_HAUT]);

translate([0,0,FOND_BASE_HAUT+FOND_BASE_HAUT/2.9]) {
    cube([FOND_BASE_LARG,FOND_BASE_LONG,FOND_BASE_HAUT]);
};

translate([30,20,FOND_BASE_HAUT+FOND_BASE_HAUT/2]) {
    cube([FOND_BASE_LARG-60,FOND_BASE_LONG-40,FOND_BASE_HAUT*2]);
};

/* PIEDS */
translate([11,50]) {
    cylinder(FOND_BASE_HAUT*2,r=RAYON_PIED);
};

translate([FOND_BASE_LARG-11,50]) {
    cylinder(FOND_BASE_HAUT*2,r=RAYON_PIED);
};

translate([FOND_BASE_LARG-11,FOND_BASE_LONG-50]) {
    cylinder(FOND_BASE_HAUT*2,r=RAYON_PIED);
};

translate([11,FOND_BASE_LONG-50]) {
    cylinder(FOND_BASE_HAUT*2,r=RAYON_PIED);
};

/* Guide PIEDS */
translate([11,50,FOND_BASE_HAUT+FOND_BASE_HAUT/4]) {
    difference() {
          cylinder(FOND_BASE_HAUT*1.5,r=      RAYON_PIED+5);
          cylinder(FOND_BASE_HAUT*1.5,r=      RAYON_PIED+1);
    }
};

translate([FOND_BASE_LARG-11,FOND_BASE_LONG-50,FOND_BASE_HAUT+FOND_BASE_HAUT/4]) {
    difference() {
        cylinder(FOND_BASE_HAUT*1.5,r=RAYON_PIED+5);
        cylinder(FOND_BASE_HAUT*1.5,r=      RAYON_PIED+1);
    }

};

translate([FOND_BASE_LARG-11,50,FOND_BASE_HAUT+FOND_BASE_HAUT/4]) {
    difference() {
        cylinder(FOND_BASE_HAUT*1.5,r=RAYON_PIED+5);
        cylinder(FOND_BASE_HAUT*1.5,r=      RAYON_PIED+1);
    }
};

translate([11,FOND_BASE_LONG-50,FOND_BASE_HAUT+FOND_BASE_HAUT/4]) {
    difference() {
    cylinder(FOND_BASE_HAUT*1.5,r=RAYON_PIED+5);
        cylinder(FOND_BASE_HAUT*1.5,r=      RAYON_PIED+1);
    }
};

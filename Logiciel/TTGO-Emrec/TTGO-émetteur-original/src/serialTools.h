void initSerial() {
#ifdef DEBUG
  Serial.begin(SERIAL_BAUDRATE);
  while (!Serial);
  Serial.println();
  Serial.println("Port série opérationnel");
#endif
};

void logSerial(String msg) {
#ifdef DEBUG
Serial.println(msg);
#endif
}

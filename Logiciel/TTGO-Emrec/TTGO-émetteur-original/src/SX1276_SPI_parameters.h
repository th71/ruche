
/*
   SX1276
    TTGO sans OLED
*/
#define SX1276_NSS           18
#define SX1276_RXTX          LMIC_UNUSED_PIN
#define SX1276_RESET         14
#define SX1276_DIO0          26
#define SX1276_DIO1          33
#define SX1276_DIO2          LMIC_UNUSED_PIN

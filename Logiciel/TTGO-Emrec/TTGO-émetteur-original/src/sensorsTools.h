
RTC_DATA_ATTR dataSetStruct data[24] = { 0 } ;

boolean initI2C() {
  Wire.begin();
  logSerial("I2C started");
  return true;
}

#ifndef SIMUL
boolean initSensors() {
  boolean result = false ;

#ifdef BME_ENABLED
  result |= initI2C() ;
  result |= initBME() ;
#endif
  
#ifdef MLX_ENABLED
  result |= initMLX() ;
#endif

#ifdef HX_ENABLED
  result |= initHX() ;
#endif
  
  return result ;
}

void getDataFromSensors() {

#ifdef BME_ENABLED
  readBME(&dataArray[0], &dataArray[1], &dataArray[2]);
  #ifdef STRUCT
  readBME(&dataSet.Temperature,&dataSet.Humidity,&dataSet.Pressure);
  #endif
#endif

#ifdef MLX_ENABLED
  dataArray[3] = readMLX() ;
  #ifdef STRUCT
  dataSet.Temperature_INT = readMLX() ;
  #endif
#endif

#ifdef HX_ENABLED
  dataArray[4] = readHX() ;
  #ifdef STRUCT
  dataSet.Weight = readHX() ;
  #endif
#endif

  #ifdef STRUCT
  data[dataIndex] = dataSet ;
  dataIndex = (dataIndex + 1)%24;
  logSerial("Index:"+String(dataIndex));
  #endif
}

#else
boolean initSensors() {
  boolean result = true ;
  return result ;
}

void getDataFromSensors() {
  dataArray[0]=10 ;
  dataArray[1]=50 ;
  dataArray[2]=1000 ;
  dataArray[3]=10 ;
  dataArray[4]=0x110022FF ;
}
#endif

void makeFrameFromData() {
  
  logSerial("Make FRAME");
  LoRaFrame[0] = 0x00 ;
  LoRaFrame[1] = ADDR ;
#ifdef BME_ENABLED
  LoRaFrame[2] = byte(128+dataArray[0]);
  LoRaFrame[3] = byte(dataArray[1]);
  LoRaFrame[4] = byte(dataArray[2]-960);
  #ifdef STRUCT
  LoRaFrame[2] = byte(128+dataSet.Temperature);
  LoRaFrame[3] = byte(dataSet.Humidity); 
  LoRaFrame[4] = byte(dataSet.Pressure-960);
  #endif
#endif
  
#ifdef MLX_ENABLED
  LoRaFrame[5] = byte(dataArray[3]);
  #ifdef STRUCT
  LoRaFrame[4] = byte(dataSet.Temperature_INT);
  #endif
#endif

#ifdef HX_ENABLED
  
  #ifdef STRUCT
  int hxValue = int(dataSet.Weight);
  #else
  int hxValue = int(dataArray[4]);
  #endif
  
  LoRaFrame[6] = (hxValue  & 0xFF000000) >> 24 ;
  LoRaFrame[7] = (hxValue & 0x00FF0000) >>  16 ;
  LoRaFrame[8] = (hxValue & 0x0000FF00) >>  8  ;
  LoRaFrame[9] = (hxValue & 0x000000FF)      ;
#endif
  LoRaFrame[10] = 0x00 ;
  LoRaFrame[11] = FRAME_LENGTH ;
  LoRaFrame[12] = 0x00 ;
  LoRaFrame[13] = 0x00 ;

  String str = String("Trame:") ;
  for (int i = 0 ; i<14 ; i++) {
    str+=String(LoRaFrame[i])+":";
      }
  logSerial(str);
}




#ifdef HX_ENABLED
HX711 scale;

boolean initHX() {
  logSerial("HX711 en cours de démarrage");
  scale.begin(PinHX_DOUT,PinHX_SCK);
  if (!scale.is_ready())
  {
    logSerial("Démarrage HX711 impossible!");
    return false ;
  }
  else
  {
    logSerial("Démarrage HX711 réalisé!");
    return true ;
  }

 
  logSerial("HX711 démarré");
  scale.set_scale(1.f);                      // this value is obtained by calibrating the scale with known weights; see the README for details
  logSerial("Paramétrage scale à 1");
  scale.set_offset(0);                // reset the scale to 0
  logSerial("Paramétrage du décalage à 0");
  return true ;
}

byte readHX() {
  scale.power_up();
  int value = scale.read_average(10) ;
  logSerial("Valeur Poids brute"+String(value));
  value = int(value/pow(2,10)) ;
  logSerial("Valeur Poids corrigée"+String(value));
  scale.power_down();
  digitalWrite(13,LOW);
  digitalWrite(16,LOW);
  return (byte)(value);
}
#endif

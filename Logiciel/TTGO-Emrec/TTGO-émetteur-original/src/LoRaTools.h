void initSX1276() {
  LoRa.setPins(SX1276_NSS, SX1276_RESET, SX1276_DIO0) ;
  logSerial("Démarrage LoRa");
  if (!LoRa.begin(DATA_FREQ)) {
    logSerial("Starting DATA LoRa failed!");
    while (1);
  }
  else
  {
    logSerial("Starting DATA LoRa ok!");
  }
}

void setTXMode() {
  logSerial("Paramétrage LoRa");
/*  LoRa.setTxPower(DATA_POWER);

  LoRa.setSpreadingFactor(DATA_SF);
  LoRa.setSignalBandwidth(DATA_BW);

  LoRa.setCodingRate4(DATA_CODING_RATE);
  LoRa.setPreambleLength(DATA_PREAMBLE);
  LoRa.setSyncWord(DATA_SYNCWORD); */
  
}

void sendFrameThroughLoRa(byte* frame) {
  logSerial("Transmission LoRa...");
  LoRa.beginPacket();
  LoRa.write(frame, FRAME_LENGTH);
  LoRa.endPacket();
}

#ifdef BME_ENABLED

uint8_t bmeI2CAddr=0x76;

BME280I2C::Settings settings(
   BME280::OSR_X1,
   BME280::OSR_X1,
   BME280::OSR_X1,
   BME280::Mode_Forced,
   BME280::StandbyTime_1000ms,
   BME280::Filter_Off,
   BME280::SpiEnable_False,
   bmeI2CAddr // I2C address. I2C specific.
);

BME280I2C bme(settings);



boolean initBME() {
  
  if (!bme.begin())
  {
    logSerial("Démarrage BME280 impossible!");
    return false ;
  }
  else
  {
    logSerial("Démarrage BME280 réalisé!");
    return true ;
  }
}

void readBME(float* dataTemp, float* dataHum, float* dataPres) {
  float temp, hum, pres ;
  
  BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
  BME280::PresUnit presUnit(BME280::PresUnit_Pa);

  bme.read(pres, temp, hum, tempUnit, presUnit);

  pres=pres/100 ;
  logSerial(
    String("BME 280:") + String(temp) + String("°C, ") +
    String(hum) + String("% ,") +
    String(pres) + String("hPa")
  );

  *dataTemp = temp ;
  *dataHum = hum ;
  *dataPres = pres ;

}
#endif

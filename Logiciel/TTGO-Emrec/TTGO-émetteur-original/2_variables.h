float dataArray[FRAME_LENGTH]    ;

#ifdef STRUCT
struct dataSetStruct {
  float Temperature = 0 ;
  float Humidity = 0 ;
  float Pressure = 0 ;
  float Temperature_INT = 0 ;
  float Weight = 0 ;
} ;

dataSetStruct dataSet ;
RTC_DATA_ATTR byte dataIndex = 0 ;

#endif

byte LoRaFrame[FRAME_LENGTH]     ;
/*
   Variables persistantes
*/
RTC_DATA_ATTR int bootCount = 0           ;
RTC_DATA_ATTR int resetCount = 0          ;
RTC_DATA_ATTR int sleepCount = 0          ;

/*
   Code TTGO du prototype ruche 2019
*/

/* === Fichier de configuration global === */
#include "configGlob.h"

/* === Bibliothèques standard === */
#include <SPI.h>
#include <Wire.h>

/* === Bibliothèques tierces === */
/* Version Sandeep Mistry */
#include <LoRa.h>

/* Version Tyler Glenn */
#include <BME280I2C.h>

/* Version Adafruit */
#include <Adafruit_MLX90614.h>

/* Version Rob Tillaart */
#include <HX711.h>

/* === Variables globables === */
byte loraBuffer[255] = { 0 };

struct dataSetStruct {
  float external_Temperature = 0 ;
  float external_Humidity = 0 ;
  float external_Pressure = 0 ;
  float internal_Temperature = 0 ;
  float target_Temperature = 0 ;
  float Weight = 0 ;
} ;

dataSetStruct dataSet ;

/*
   Variables persistantes
*/
RTC_DATA_ATTR int bootCount = 0           ;
RTC_DATA_ATTR int resetCount = 0          ;
RTC_DATA_ATTR int sleepCount = 0          ;
RTC_DATA_ATTR byte frameId = 0 ;

/* === Bibliothèques outils === */
#ifdef DEBUG
#include "src/serialTools.h"
#endif

#include "src/SX1276_SPI_parameters.h"
#include "src/LoRaTools.h"

#include "src/bmeTools.h"
#include "src/mlxTools.h"
#include "src/hxTools.h"

#include "src/sensorsTools.h"

#include "src/sleepTools.h"

#include "src/powerParam.h"

/* === Programme principal === */
void setup() {
  ++bootCount;

  /*
  Paramètres le broches en entrée et désactive l'ADC pour essayer de
  réduire la consommation électrique
  */
  powerParam() ;  
  #ifdef DEBUG
   initSerial();
  print_wakeup_reason();
  #endif
   
  initSensors() ;
  initSX1276() ;
}

void loop() {  
  #ifdef DEBUG
  logSerial("Itération avec transmission ");
  #endif 
  
  getDataFromSensors(dataSet) ;
  makeFrameFromData(loraBuffer, dataSet) ;

  setTXMode() ;
  sendFrameThroughLoRa(loraBuffer) ;

  LoRa.end();
  LoRa.sleep();
  fallInSleep();
}

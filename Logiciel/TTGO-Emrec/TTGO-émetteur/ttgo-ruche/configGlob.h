/* === Fichier de configuration global === */

#define SKETCH_NAME  "Récepteur ruche 31/05/2021"

//#define DEV
#ifdef DEV
  #define DEBUG
  #define SIMUL
#endif

/* SERIE et DEBUG */
#ifdef DEBUG
  #define SERIAL_BAUDRATE 115200
#endif

#define STRUCT
#define ADDR 1

/* MODE SOMMEIL */
#define TIME_TO_SLEEP  60

/* LORA MONTANT */
#define DATA_POWER 14
#define DATA_FREQ 868.1E6
#define DATA_SF 7
#define DATA_BW  125
#define DATA_CODING_RATE 5
#define DATA_PREAMBLE 8
#define DATA_SYNCWORD 0x34

/* --- Broches --- */
#define PinI2C_SDA  21
#define PinI2C_SCL  20
#define PinHX_DOUT  16
#define PinHX_SCK   13

#define SX1276_NSS           18
#define SX1276_RXTX          LMIC_UNUSED_PIN
#define SX1276_RESET         14
#define SX1276_DIO0          26
#define SX1276_DIO1          33
#define SX1276_DIO2          LMIC_UNUSED_PIN

/* TRAME */
#define FRAME_LENGTH_BASE 15

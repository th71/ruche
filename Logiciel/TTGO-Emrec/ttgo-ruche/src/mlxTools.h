#ifdef MLX_ENABLED
Adafruit_MLX90614 mlx = Adafruit_MLX90614();

boolean initMLX() {
  return (!mlx.begin());
}

byte readMLX() {
  logSerial("Ambient = " + String(mlx.readAmbientTempC()));
  logSerial("\tObject = " + String(mlx.readObjectTempC()));

  return byte(mlx.readObjectTempC());
}
#endif

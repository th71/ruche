void initSX1276() {
  
}

void setTXMode() {
  logSerial("Paramétrage LoRa");
/*  LoRa.setTxPower(DATA_POWER);

  LoRa.setSpreadingFactor(DATA_SF);
  LoRa.setSignalBandwidth(DATA_BW);

  LoRa.setCodingRate4(DATA_CODING_RATE);
  LoRa.setPreambleLength(DATA_PREAMBLE);
  LoRa.setSyncWord(DATA_SYNCWORD); */
  
}

void sendFrameThroughLoRa(byte* frame) {
  LoRa.setPins(SX1276_NSS, SX1276_RESET, SX1276_DIO0) ;
  #ifdef DEBUG
    logSerial("Démarrage LoRa");
  #endif
  if (!LoRa.begin(DATA_FREQ)) {
    #ifdef DEBUG
      logSerial("Starting DATA LoRa failed!");
    #endif
    while (1);
  }
  else
  {
    #ifdef DEBUG
      logSerial("Starting DATA LoRa ok!");
    #endif
  }
  #ifdef DEBUG
    logSerial("Transmission LoRa...");
  #endif
  LoRa.beginPacket();
  LoRa.write(frame, FRAME_LENGTH);
  LoRa.endPacket();
  LoRa.end();
  #ifdef DEBUG
    logSerial("Arrêt LoRa");
  #endif
  LoRa.sleep();
  #ifdef DEBUG
    logSerial("LoRa en sommeil");
  #endif

}

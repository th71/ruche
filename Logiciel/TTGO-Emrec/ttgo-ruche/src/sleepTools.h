#include "driver/rtc_io.h"
#include "esp_sleep.h"

void print_wakeup_reason(){
  esp_sleep_wakeup_cause_t wakeup_reason;

  wakeup_reason = esp_sleep_get_wakeup_cause();
  
  switch(wakeup_reason)
  {
  case 0  :
    {
      ++resetCount ;
      logSerial("...RESET");
      delay(2);
    } break;
  
    case 4  :
    {
      ++sleepCount ;
      logSerial("Réveil par chronomètre");
      delay(2);
    } break;
    default :
    {
      ++sleepCount ;
      logSerial("Réveil pour cause inconnue");
      delay(2);
    } break;
  }
  logSerial("Cause de réveil n°"+String(wakeup_reason)+"\t"
	    +String(bootCount)+"ième redémarrage\t"
	    +String(resetCount)+" RAZ\t"
	    +String(sleepCount)+" REVEILS");
}

void fallInSleep() {
  esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_OFF);
  logSerial("Désactivation des périphériques RTC durant la période de sommeil");
  
  logSerial("ESP32 configuré pour le réveiller après " + String(TIME_TO_SLEEP) + " secondes.");
  logSerial("Bonne nuit!");
  Serial.flush();

  esp_deep_sleep(TIME_TO_SLEEP * 1000000);
}

#include "driver/adc.h"

void powerParam() {
  adc_power_off();  //Extinction ADC pour économie d'énergie

  pinMode(0, INPUT) ;
  pinMode(22, INPUT) ;
  pinMode(19, INPUT) ;
  pinMode(23, INPUT) ;
  pinMode(18, INPUT) ;
  pinMode(5, INPUT) ;
  pinMode(15, INPUT) ;
  pinMode(2, INPUT) ;
  pinMode(4, INPUT) ;
  pinMode(17, INPUT) ;
  pinMode(16, INPUT) ;

  pinMode(36, INPUT) ;
  pinMode(37, INPUT) ;
  pinMode(34, INPUT) ;
  pinMode(35, INPUT) ;
  pinMode(32, INPUT) ;
  pinMode(33, INPUT) ;
  pinMode(25, INPUT) ;
  pinMode(26, INPUT) ;
  pinMode(27, INPUT) ;
  pinMode(14, INPUT) ;
  pinMode(12, INPUT) ;
  pinMode(13, INPUT) ;
  pinMode(21, INPUT) ;

  digitalWrite(0, LOW) ;
  digitalWrite(22, LOW) ;
  digitalWrite(19, LOW) ;
  digitalWrite(23, LOW) ;
  digitalWrite(18, LOW) ;
  digitalWrite(5, LOW) ;
  digitalWrite(15, LOW) ;
  digitalWrite(2, LOW) ;
  digitalWrite(4, LOW) ;
  digitalWrite(17, LOW) ;
  digitalWrite(16, LOW) ;

  digitalWrite(36, LOW) ;
  digitalWrite(37, LOW) ;
  digitalWrite(34, LOW) ;
  digitalWrite(35, LOW) ;
  digitalWrite(32, LOW) ;
  digitalWrite(33, LOW) ;
  digitalWrite(25, LOW) ;
  digitalWrite(26, LOW) ;
  digitalWrite(27, LOW) ;
  digitalWrite(14, LOW) ;
  digitalWrite(12, LOW) ;
  digitalWrite(13, LOW) ;
  digitalWrite(21, LOW) ;
 
}

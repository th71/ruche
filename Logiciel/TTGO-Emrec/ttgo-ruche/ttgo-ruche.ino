/*
   Code TTGO du prototype ruche
   Version 01/06/2021
*/

/* === Fichier de configuration global === */
#include "configGlob.h"

/* === Bibliothèques standard === */
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>

/* === Bibliothèques tierces === */
/* Version Sandeep Mistry */
#include <LoRa.h>

/* Version Tyler Glenn */
#include <Adafruit_BME280.h>

/* Version Adafruit */
#include <Adafruit_MLX90614.h>

/* Version Rob Tillaart */
#include <HX711.h>

/* === Variables globables === */
struct dataSetStruct {
  float external_Temperature = 0 ;
  float external_Humidity = 0 ;
  float external_Pressure = 0 ;
  float internal_Temperature = 0 ;
  float target_Temperature = 0 ;
  float Weight = 0 ;
} ;


/*
   Variables persistantes
*/
RTC_DATA_ATTR byte frameId = 0 ;

/* === Bibliothèques outils === */
#ifdef DEBUG
#include "src/serialTools.h"
#endif

#include "src/SX1276_SPI_parameters.h"
#include "src/LoRaTools.h"

BME280I2C bme();

boolean initBME() {
  
  if (!bme.begin())
  {
    logSerial("Démarrage BME280 impossible!");
    return false ;
  }
  else
  {
    logSerial("Démarrage BME280 réalisé!");
    return true ;
  }
}

void readBME(float* dataTemp, float* dataHum, float* dataPres) {
  float temp, hum, pres ;
  
  BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
  BME280::PresUnit presUnit(BME280::PresUnit_Pa);

  bme.read(pres, temp, hum, tempUnit, presUnit);

  pres=pres/100 ;
  logSerial(
    String("BME 280:") + String(temp) + String("°C, ") +
    String(hum) + String("% ,") +
    String(pres) + String("hPa")
  );

  *dataTemp = temp ;
  *dataHum = hum ;
  *dataPres = pres ;

}

#include "src/mlxTools.h"
#include "src/hxTools.h"
byte initI2C() {
  byte status = Wire.begin();
  #ifdef DEBUG
  logSerial("I2C started");
  #endif
  return status;
}

boolean initSensors() {
  return status ;
}

dataSetStruct getDataFromSensors() {

  dataSetStruct incomingData ;
  
  
  
  incomingData.internal_Temperature = readMLX() ;

  dataSet.Weight = readHX() ;

  #ifdef DEBUG
  logSerial("Index:"+String(dataIndex));
  #endif
  return incomingData;
}

void makeFrameFromData() {
  #ifdef DEBUG
    logSerial("Make FRAME");
  #endif
  
  
  #ifdef DEBUG
  String str = String("Trame:") ;
  for (int i = 0 ; i<14 ; i++) {
    str+=String(LoRaFrame[i])+":";
      }
  logSerial(str);
  #endif
}



#include "src/sleepTools.h"

#include "src/powerParam.h"

/* === Programme principal === */
void setup() {
  ++bootCount;
  dataSetStruct dataSet ;
  
  /*
  Paramètres le broches en entrée et désactive l'ADC pour essayer de
  réduire la consommation électrique
  */
  #ifdef DEBUG
    initSerial();
    print_wakeup_reason();
  #endif


  boolean status = false ;
  while(!status) {
    status = initI2C() ;
  }

  status = initBME() ;
  if (!status) {
    dataSet.external_temperature = 255;
    dataSet.external_moisture = 255;
    dataSet.external_pressure = 255;
  }
  else {
    dataSet.external_temperature = bme.readTemperature();
    dataSet.external_moisture = bme.readHumidity();
    dataSet.external_pressure = bme.readPressure() / 100.0F;
  }
  
  status = initMLX() ;
  if (!status) {
    dataSet.internal_Temperature = 255;
    dataSet.target_Temperature = 255;
  }
  else {
    dataSet.internal_Temperature = mlx.readAmbientTempC();
    dataSet.target_Temperature = mlx.readObjectTempC();
  }
  
  status = initHX() ;
  dataSet.Weight = readHX() ;
  
  initSX1276() ;
  LoRa.beginPacket();
  LoRa.print(0x00);
  LoRa.print(ADDR);
  LoRa.print(byte(128+dataSet.external_Temperature));
  LoRa.print(byte(dataSet.external_Humidity)); 
  LoRa.print(byte(dataSet.external_Pressure-960));
  LoRa.print(byte(dataSet.internal_Temperature));
  LoRa.print(byte(dataSet.target_Temperature));
  LoRa.print(dataSet.Weight);
  LoRa.print(counter);
  LoRa.print(FRAME_LENGTH);
  LoRa.print(0x00)
  LoRa.print(0x00)
  LoRa.endPacket();
  // Le paquet est ici envoyé
  LoRa.end();
  LoRa.sleep();
  
  setTXMode() ;

}

void loop() {  
  #ifdef DEBUG
  logSerial("Itération avec transmission ");
  #endif 
  
  dataSet = getDataFromSensors() ;
  loraBuffer = makeFrameFromData(dataSet) ;
  setTXMode() ;
  sendFrameThroughLoRa(loraBuffer) ;

  fallInSleep();
}

void loop() {  
}

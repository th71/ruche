/* === Fichier de configuration global === */

#define SKETCH_NAME  "Récepteur ruche Version 01/06/2021"

#define DEBUG
//#define SIMUL 
/* SERIE et DEBUG */
#define SERIAL_BAUDRATE 115200

#define ADDR 1

/* MODE SOMMEIL */
#define SLEEP_ENABLED
#define TIME_TO_SLEEP  60

/* LORA MONTANT */
#define DATA_POWER 14 
#define DATA_FREQ 868.1E6
#define DATA_SF 7
#define DATA_BW  125
#define DATA_CODING_RATE 5
#define DATA_PREAMBLE 8
#define DATA_SYNCWORD 0x12


/* --- Broches --- */
// BME380 et MLX90614
#define PinI2C_SDA  21
#define PinI2C_SCL  20
// HX711
#define PinHX_DOUT  16
#define PinHX_SCK   13
// SX1276 (LORA)
#define SX1276_NSS           18
#define SX1276_RXTX          LMIC_UNUSED_PIN
#define SX1276_RESET         14
#define SX1276_DIO0          26
#define SX1276_DIO1          33
#define SX1276_DIO2          LMIC_UNUSED_PIN

/* TRAME */
// 00|ID|EXT-T|EXT-H|EXT-P|INT-T|OBJ-T|POIDS4|POIDS3|POIDS2|POIDS1|00|Longueur|00|00
#define FRAME_LENGTH 15

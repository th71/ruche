/* === Fichier de configuration global === */

/* SERIE et DEBUG */
#define SERIAL_BAUDRATE 115200
#define DEBUG

/* Data memort */
#define DATA_ARRAY_LENGTH 15000

/* LORA */
#define FRAME_LENGTH 11

/* LORA DATA */
#define DATA_POWER 14
#define DATA_FREQ 868.1E6
#define DATA_SF 7
#define DATA_BW  125
#define DATA_CODING_RATE 5
#define DATA_PREAMBLE 8
#define DATA_SYNCWORD 0x12

/* LORA ACK */
#define ACK_POWER 14 
#define ACK_FREQ 868.1E6
#define ACK_SF 7
#define ACK_BW  125
#define ACK_CODING_RATE 5
#define ACK_PREAMBLE 8
#define ACK_SYNCWORD 0x12

#ifdef ESP32
#define SX1276_NSS           18
#define SX1276_RXTX          LMIC_UNUSED_PIN
#define SX1276_RESET         14
#define SX1276_DIO0          26
#define SX1276_DIO1          33
#define SX1276_DIO2          LMIC_UNUSED_PIN
#endif

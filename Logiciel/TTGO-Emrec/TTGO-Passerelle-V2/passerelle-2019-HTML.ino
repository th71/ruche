/*
   Code LG01-P du prototype ruche 2019
*/

/* === Bibliothèques standard === */
#ifndef ESP32
#include <Console.h>
#include <Bridge.h>

#include <BridgeServer.h>
#include <BridgeClient.h>
BridgeServer server;
#else
#warning "ESP32 COMPILER"
#define Console Serial
#define Bridge Serial
#endif

// Listen on default port 5555, the webserver on the Yún
// will forward there all the HTTP requests for us.

#include <SPI.h>
#include <LoRa.h>

#include <WiFi.h>
/* === Fichier de configuration global === */
#include "config.h"

byte RXBuffer[256] = {0} ;
byte DATABuffer[DATA_ARRAY_LENGTH * 5] = {0};

unsigned long dataIndex = 0 ;

String startString;

long hits = 0;

char ssid[] = "Bbox-A210A0";      // your network SSID (name)
char pass[] = "991D2C134E";   // your network password

WiFiServer server(8888);
WiFiClient client ;
/* === Bibliothèques outils === */
/* ==> PORT CONSOLE ET DEBOGGAGE <== */
#ifdef DEBUG
void initSerial() {
#ifndef ESP32
  Console.begin();
  while (!Console);
  Console.println();
  Console.println("Console série opérationnelle");
#else
  Serial.begin(115200);
  Serial.println("Console série opérationnelle");
#endif
  SPI.begin();
}

void logSerial(String msg) {
  Console.println(msg);
}
#else
void initSerial() {
}
void logSerial(String msg) {
}
#endif

/* ==> OUTILS LORA <== */
void loraInit() {
  logSerial("Démarrage de SX1276 sur fréquence " + String(DATA_FREQ));
  LoRa.setPins(SX1276_NSS, SX1276_RESET, SX1276_DIO0) ;
  if (!LoRa.begin(DATA_FREQ)) {
    logSerial("SX1276 en erreur!");
    while (1);
  }
  else {
    logSerial("SX1276 en fonction!");
  }
}

void setRXmode() {
  logSerial("Bascule en mode réception") ;
  /*  logSerial("POWER") ;
    LoRa.setTxPower(DATA_POWER);
    logSerial("SF") ;
    LoRa.setSpreadingFactor(DATA_SF);
    logSerial("BW") ;
    LoRa.setSignalBandwidth(DATA_BW);
    logSerial("CR") ;
    LoRa.setCodingRate4(DATA_CODING_RATE);
    logSerial("PREAMBLE") ;
    LoRa.setPreambleLength(DATA_PREAMBLE);
    logSerial("SYNCWORD") ;
    LoRa.setSyncWord(DATA_SYNCWORD);
  */
  logSerial("Mode réception configuré");
}

void waitForPacket() {
  logSerial("À l'écoute de paquets");
  boolean noPacketReceived = true ;

  while (noPacketReceived) {

    byte packetSize = LoRa.parsePacket();
    if (packetSize) {
      Console.print("Paquet reçu");
      byte index = 0 ;
      while (LoRa.available()) {
        RXBuffer[index] =  LoRa.read();
        Console.print(RXBuffer[index], DEC);
        Console.print(",");
        index++ ;
      }
      Console.print(" de longueur ") ;
      Console.print(index);
      Console.print("'  with RSSI ");
      Console.println(LoRa.packetRssi());
      Console.println(" * ---- *");
      noPacketReceived = false ;
    }


  }
}


void sendAck() {
  LoRa.beginPacket();
  LoRa.print("ACK");
  LoRa.endPacket();

}

void processPacket() {
  if (((RXBuffer[0] == 0) || (RXBuffer[1] == 0) || (RXBuffer[7] == 0) || (RXBuffer[9] == 0) || (RXBuffer[10] == 0)) && (RXBuffer[8] == FRAME_LENGTH)) {
    logSerial("Trame reçue bien formée") ;
#ifndef ESP32
    Process p;    // Create a Linux Process
    String command = "/usr/bin/python /root/data.py " ;
    for (int i = 2 ; i < 7 ; i++) {
      command += String(RXBuffer[i]) + " ";
      DATABuffer[dataIndex + i - 2] = RXBuffer[i] ;
    }
    dataIndex += 5 ;
    logSerial(String("Nouvel index:") + String(dataIndex));
    logSerial("Commande shell:" + command) ;
    p.runShellCommand(command);  // Process that launch the "lora_udp_fwd" command
    logSerial("Terminées") ;
#else
    String command = "/usr/bin/python /root/data.py " ;
    for (int i = 2 ; i < 7 ; i++) {
      command += String(RXBuffer[i]) + " ";
      DATABuffer[dataIndex + i - 2] = RXBuffer[i] ;
    }
    dataIndex += 5 ;
    logSerial(String("Nouvel index:") + String(dataIndex));

    logSerial("Commande shell:" + command) ;
#endif

  }
  else {
    logSerial("Trame reçue mal formée") ;
    logSerial(String(RXBuffer[0] == 0)) ;
    logSerial(String(RXBuffer[1] == 0)) ;
    logSerial(String((RXBuffer[0] == 0) || (RXBuffer[1] == 0))) ;
    logSerial(String(RXBuffer[7])) ;
    logSerial(String(RXBuffer[9])) ;
    logSerial(String(RXBuffer[10])) ;
    logSerial(String(RXBuffer[8] == FRAME_LENGTH)) ;
  }
}





void setTXmode() {
  /*
    LoRa.setTxPower(ACK_POWER);

    LoRa.setSpreadingFactor(ACK_SF);
    LoRa.setSignalBandwidth(ACK_BW);

    LoRa.setCodingRate4(ACK_CODING_RATE);
    LoRa.setPreambleLength(ACK_PREAMBLE);
    LoRa.setSyncWord(ACK_SYNCWORD);

    if (!LoRa.begin(ACK_FREQ)) {
    logSerial("Starting ACK LoRa failed!");
    while (1);
    }
    else
    {
    logSerial("Starting ACK LoRa ok!");
    }

  */

}

void initBridge() {
  Bridge.begin(SERIAL_BAUDRATE);
}

void setup() {
  initBridge();
  initSerial();
  loraInit();

#ifndef ESP32
  // Listen for incoming connection only from localhost
  // (no one from the external network could connect)
  server.listenOnLocalhost();
  server.begin();

  // get the time that this sketch started:
  Process startTime;
  startTime.runShellCommand("date");
  while (startTime.available()) {
    char c = startTime.read();
    startString += c;
  }
  //  processPacket();
#else
  int status = WL_IDLE_STATUS;
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    status = WiFi.begin(ssid, pass);

    // wait 10 seconds for connection:
    delay(10000);
  }
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
  server.begin();

#endif
}

void loop() {
  setRXmode() ;
  waitForPacket() ;
  setTXmode() ;
  sendAck() ;
  processPacket() ;
#ifndef ESP32
  BridgeClient client = server.accept();

  // There is a new client?
  if (client) {
    // Process request

    // Close connection and free resources.
    client.stop();
  }

  String command = client.readStringUntil('/');
  logSerial(command);
  for (int i = 0 ; i < 7 ; i++) {
    client.print("Variable: ");
    client.print(String(i) + "=>" + String(RXBuffer[i]));
  }
  logSerial("BC....");
  client.stop();
  delay(1000);
#else
  client = server.available();
  if (client) {
    Serial.println("new client");
    // an http request ends with a blank line
    bool currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        Serial.write(c);
        // if you've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so you can send a reply
        if (c == '\n' && currentLineIsBlank) {
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
          client.println("Refresh: 5");  // refresh the page automatically every 5 sec
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");

          // output the value of each analog input pin
          for (int index = 0 ; index < dataIndex; index += 5) {
            client.print(index / 5);
            client.print(",");

            for (int pos = 0 ; pos < 5 ; pos++) {
              client.print(DATABuffer[index + pos]);
              client.print(",");

            }
            client.println("<br />");
          }
          client.println("</html>");
          break;
        }
        if (c == '\n') {
          // you're starting a new line
          currentLineIsBlank = true;
        } else if (c != '\r') {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(1);

    // close the connection:
    client.stop();
    Serial.println("client disonnected");
  }

#endif
}

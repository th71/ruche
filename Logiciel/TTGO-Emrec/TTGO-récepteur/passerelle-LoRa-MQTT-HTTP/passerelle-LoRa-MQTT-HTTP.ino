#include "configGlob.h"
#include <WiFi.h>
#include <WiFiClient.h>
#include <SPI.h>
#include <LoRa.h>
#include <PubSubClient.h>
#include <ArduinoHttpClient.h>

void init_serial() {
  Serial.begin(115200) ;
  Serial.print("Sortie série initialisée") ;
};

void init_wifi() {
  Serial.print("Démarrage du WiFi");

  WiFi.begin(mySSID, myPSK) ;
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000) ;
  } ;

  Serial.print("WiFi opérationnel sur l'adresse :") ;
  Serial.println(WiFi.localIP()) ;
} ;

void init_lora() {
  Serial.print("Initialisation du bus SPI");
  SPI.begin();		       
     
  Serial.print("Démarrage de SX1276 sur fréquence " + String(DATA_FREQ));

  LoRa.setPins(SX1276_NSS,
               SX1276_RESET,
               SX1276_DIO0) ;

  if (!LoRa.begin(DATA_FREQ)) {
    Serial.print("SX1276 en erreur!");
    while (1);
  }
  else {
    Serial.print("SX1276 en fonction!");
  }
} ;

byte RXBuffer[255] ;

void wait_lora_packet() {
  Serial.print("Attente d'un paquet");

  while (not(LoRa.available())) {
      delay(1000) ;
    } ;
  
  byte packetSize = LoRa.parsePacket();
  Serial.print("Paquet reçu de taille");
  Serial.print(packetSize) ;
  byte index = 0 ;
  while (LoRa.available()) {
    RXBuffer[index] =  LoRa.read();
    Serial.print(RXBuffer[index], DEC);
    Serial.print(",");
    index++ ;
  }
  Serial.print(String(" [RSSI:") + String(LoRa.packetRssi()) +
	       String(",SNR:") + String(LoRa.packetSnr()) +
	       String(",FRQ ERROR:") + String(LoRa.packetFrequencyError()) +
	       String("]")
               );
  
  Serial.println(" * ---- * ");
};

boolean validate_lora_packet() {
   return (
           (RXBuffer[0] == 0) &&
           (RXBuffer[1] < DEV_ADDR_MAX) &&
           (RXBuffer[11] == 0) &&
           (RXBuffer[12] == FRAME_LENGTH) &&
           (RXBuffer[13] == 0) &&
           (RXBuffer[14] == 0)
         ) ;
} ;


String lora2mqtt_message() {
  String dataString = "";
  for (int i = 1 ; i < 7 ; i++) {
    dataString += String(RXBuffer[i]) + ",";
  }

  float hxValue = 16777216 * RXBuffer[7] +
                  65536 * RXBuffer[8] +
                  256 * RXBuffer[9] +
                  RXBuffer[10] ;
  dataString += String(hxValue, 0) ;
  dataString.replace(" ", "");
  return dataString;
}

WiFiClient ipClient ;
PubSubClient MQTT(ipClient) ;
IPAddress mqttBrocker(127,0,0,1) ;

void send_mqtt_message() {
  String dataString = lora2mqtt_message() ;
  Serial.print("Transmission MQTT de " + dataString) ;
  char buffer[255] = {0};
  for (byte i = 0; i < dataString.length(); i++) {
    buffer[i] = (char)dataString[i];
  }

  boolean status = 0 ;

  MQTT.setServer(mqttBrocker, 1883) ;
  status = MQTT.connect("");
  if (!(status)) {
    Serial.print("Impossible de contacter le courtier") ;
  }
  status = MQTT.publish("DRAGINO", buffer) ;
  if (!(status)) {
    Serial.print("Impossible de publier") ;
  }
  MQTT.disconnect() ;

};

HttpClient webclient = HttpClient(ipClient, "127.0.0.1", 5000);
void send_http_message() {
  webclient.get("http://localhost:5000/test");
} ;

/* Programme principal */

void setup() {
  init_serial() ;
  init_wifi() ;
  init_lora() ;
};

void loop() {
  wait_lora_packet() ;
  validate_lora_packet() ;
  
  send_mqtt_message() ;
  send_http_message() ;
};

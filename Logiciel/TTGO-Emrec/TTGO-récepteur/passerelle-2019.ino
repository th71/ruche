/*
   Code LG01-P du prototype ruche 2019
*/

#define SDA  21;
#define SCL  22;

/* === Bibliothèques standard === */
#ifndef ESP32

#include <FileIO.h>
#include <Console.h>
#include <Bridge.h>

#include <BridgeServer.h>
#include <BridgeClient.h>
BridgeServer server;
#else
#warning "ESP32 COMPILER"
#define Console Serial
#define Bridge Serial
#endif

// Listen on default port 5555, the webserver on the Yún
// will forward there all the HTTP requests for us.

#include <SPI.h>
#include <LoRa.h>

#include <WiFi.h>
/* === Fichier de configuration global === */
#include "configGlob.h"

byte RXBuffer[256] = {0} ;

String startString;

long hits = 0;

/* === Bibliothèques outils === */
/* ==> PORT CONSOLE ET DEBOGGAGE <== */
#ifdef DEBUG
void initSerial() {
#ifndef ESP32
  Console.begin();
  while (!Console);
  Console.println();
  Console.println("Console série opérationnelle");
#else
  Serial.begin(115200);
  Serial.println("Console série opérationnelle");
#endif
  SPI.begin();
}

void logSerial(String msg) {
  Console.println(msg);
}
#else
void initSerial() {
}
void logSerial(String msg) {
}
#endif

/* ==> OUTILS LORA <== */
void loraInit() {
  logSerial("Démarrage de SX1276 sur fréquence " + String(DATA_FREQ));
#ifdef ESP32
  LoRa.setPins(SX1276_NSS, SX1276_RESET, SX1276_DIO0) ;
#endif
  if (!LoRa.begin(DATA_FREQ)) {
    logSerial("SX1276 en erreur!");
    while (1);
  }
  else {
    logSerial("SX1276 en fonction!");
  }
}

void setRXmode() {
  logSerial("Bascule en mode réception") ;
  /*  logSerial("POWER") ;
    LoRa.setTxPower(DATA_POWER);
    logSerial("SF") ;
    LoRa.setSpreadingFactor(DATA_SF);
    logSerial("BW") ;
    LoRa.setSignalBandwidth(DATA_BW);
    logSerial("CR") ;
    LoRa.setCodingRate4(DATA_CODING_RATE);
    logSerial("PREAMBLE") ;
    LoRa.setPreambleLength(DATA_PREAMBLE);
    logSerial("SYNCWORD") ;
    LoRa.setSyncWord(DATA_SYNCWORD);
  */
  logSerial("Mode réception configuré");
}

void waitForPacket() {
  logSerial("À l'écoute de paquets");
  boolean noPacketReceived = true ;

  while (noPacketReceived) {

    byte packetSize = LoRa.parsePacket();
    if (packetSize) {
      Console.print("Paquet reçu");
      byte index = 0 ;
      while (LoRa.available()) {
        RXBuffer[index] =  LoRa.read();
        Console.print(RXBuffer[index], DEC);
        Console.print(",");
        index++ ;
      }
      Console.print(" de longueur ") ;
      Console.print(index);
      Console.print("'  with RSSI ");
      Console.println(LoRa.packetRssi());
      Console.println(" * ---- *");
      noPacketReceived = false ;
    }


  }
}


void sendAck() {
  LoRa.beginPacket();
  LoRa.print("ACK");
  LoRa.endPacket();

}

void processPacket() {
  if (((RXBuffer[0] == 0) && (RXBuffer[1] == 0) && (RXBuffer[10] == 0) && (RXBuffer[12] == 0) && (RXBuffer[13] == 0)) && (RXBuffer[11] == FRAME_LENGTH)) {
    logSerial("Trame reçue bien formée") ;
#ifndef ESP32
    Process p;    // Create a Linux Process
    String command = "/usr/bin/python /root/data.py " ;
    for (int i = 1 ; i < 6 ; i++) {
      command += String(RXBuffer[i]) + " ";
    }
    int hxValue = (((int)RXBuffer[6]) << 32) + (((int)RXBuffer[7]) << 16) + (((int)RXBuffer[8]) << 8) + (RXBuffer[9]) ;
    command += String(hxValue);
    logSerial("Commande shell:" + command) ;
    p.runShellCommand(command);  // Process that launch the "lora_udp_fwd" command
    logSerial("Terminées") ;
#else
    String command = "/usr/bin/python /root/data.py " ;
    for (int i = 2 ; i < 7 ; i++) {
      command += String(RXBuffer[i]) + " ";
    }
    logSerial("Commande shell:" + command) ;
#endif

  }
  else {
    logSerial("Trame reçue mal formée") ;
    logSerial(String(RXBuffer[0] == 0)) ;
    logSerial(String(RXBuffer[1] == 0)) ;
    logSerial(String((RXBuffer[0] == 0) || (RXBuffer[1] == 0))) ;
    logSerial(String(RXBuffer[7])) ;
    logSerial(String(RXBuffer[9])) ;
    logSerial(String(RXBuffer[10])) ;
    logSerial(String(RXBuffer[8] == FRAME_LENGTH)) ;
  }
}





void setTXmode() {
  /*
    LoRa.setTxPower(ACK_POWER);

    LoRa.setSpreadingFactor(ACK_SF);
    LoRa.setSignalBandwidth(ACK_BW);

    LoRa.setCodingRate4(ACK_CODING_RATE);
    LoRa.setPreambleLength(ACK_PREAMBLE);
    LoRa.setSyncWord(ACK_SYNCWORD);

    if (!LoRa.begin(ACK_FREQ)) {
    logSerial("Starting ACK LoRa failed!");
    while (1);
    }
    else
    {
    logSerial("Starting ACK LoRa ok!");
    }

  */

}

void initBridge() {
  Bridge.begin(SERIAL_BAUDRATE);
}

void writeVersion()
{
  File fw_version = FileSystem.open("/var/avr/fw_version", FILE_WRITE);
  fw_version.print(SKETCH_NAME);
  fw_version.close();
}

void setup() {
  initBridge();
  initSerial();

  FileSystem.begin();  
  writeVersion();
  
  loraInit();

#ifndef ESP32
  // Listen for incoming connection only from localhost
  // (no one from the external network could connect)
  server.listenOnLocalhost();
  server.begin();

  // get the time that this sketch started:
  Process startTime;
  startTime.runShellCommand("date");
  while (startTime.available()) {
    char c = startTime.read();
    startString += c;
  }
  //  processPacket();
#endif
}

void loop() {
  setRXmode() ;
  waitForPacket() ;
  setTXmode() ;
  sendAck() ;
  processPacket() ;
#ifndef ESP32
  BridgeClient client = server.accept();

  // There is a new client?
  if (client) {
    // Process request

    // Close connection and free resources.
    client.stop();
  }

  String command = client.readStringUntil('/');
  logSerial(command);
  for (int i = 0 ; i < 7 ; i++) {
    client.print("Variable: ");
    client.print(String(i) + "=>" + String(RXBuffer[i]));
  }
  logSerial("BC....");
  client.stop();
  delay(1000);
#endif
}

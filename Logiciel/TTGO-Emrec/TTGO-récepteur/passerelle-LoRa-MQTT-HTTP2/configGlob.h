/* === Fichier de configuration global === */

#define SKETCH_NAME  "Récepteur ruche V1"
const String Sketch_Ver = "Récepteur_TTGO_V2021";


/* SERIE et DEBUG */
#define SERIAL_BAUDRATE 115200
#define DEBUG
#define MQTTDEBUG
//#define SIMUL 

/* IDENTIFICATION */
#define DEV_ADDR  1
#define DEV_ADDR_MAX  255

/* MODE SOMMEIL */
#define SLEEP_ENABLED
#define TIME_TO_SLEEP  30
#define ITER_DURATION  15
#define nb_Iter  2

/* WIFI */
#define mySSID "ASUS_48_2G_24rJ"
#define myPSK  "TiGwened24rJ56000Gw"

/* LORA MONTANT */
#define DATA_POWER 14 
#define DATA_FREQ 868.1E6
#define DATA_SF 7
#define DATA_BW  125
#define DATA_CODING_RATE 5
#define DATA_PREAMBLE 8
#define DATA_SYNCWORD 0x12

/* LORA DESCENDANT */
#define ACK_POWER 14 
#define ACK_FREQ 868.1E6
#define ACK_SF 7
#define ACK_BW  125
#define ACK_CODING_RATE 5
#define ACK_PREAMBLE 8
#define ACK_SYNCWORD 0x12

/* --- CAPTEURS --- */
#define BME_ENABLED
#define HX_ENABLED
#define MLX_ENABLED

/* --- Broches --- */
#define PinI2C_SDA  21
#define PinI2C_SCL  20
#define PinHX_DOUT  16
#define PinHX_SCK   13
#ifdef ESP32
#define SX1276_NSS           18
#define SX1276_RXTX          LMIC_UNUSED_PIN
#define SX1276_RESET         14
#define SX1276_DIO0          26
#define SX1276_DIO1          33
#define SX1276_DIO2          LMIC_UNUSED_PIN
#endif

/* TRAME */
#define FRAME_LENGTH_BASE 6

#ifdef BME_ENABLED
#undef FRAME_LENGTH
#define FRAME_LENGTH FRAME_LENGTH_BASE+3
#endif

#ifdef MLX_ENABLED 
#undef FRAME_LENGTH
#define FRAME_LENGTH FRAME_LENGTH_BASE+5
#endif

#ifdef HX_ENABLED
#undef FRAME_LENGTH
#define FRAME_LENGTH FRAME_LENGTH_BASE+9
#endif

/* HTTP */
#define HTTP_PORT  80
#define HTTP_METHOD "POST"
#define HOST_NAME "example.phpoc.com"
#define PATH_NAME ""

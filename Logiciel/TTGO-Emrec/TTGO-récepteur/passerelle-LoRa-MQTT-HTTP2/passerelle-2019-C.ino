E/*
   Code TTGO du prototype ruche 2021
   =================================
   - Réception par LoRa
   - Transmission MQTT vers courtier local
   - Transmission MQTT vers service tiers
   - Transmission HTTP vers serveur local (application Flask)
   =================================
*/
/* === Bibliothèques standard === */
/* Pilotage LORA */
#include <SPI.h>
#include <LoRa.h>
/* Pilotage Wifi */
#include <WiFi.h>
/* Communication MQTT */
#include <PubSubClient.h>
/* Communication HTTP */
#include <WiFiClient.h>
/* === Fichier de configuration global === */
#include "configGlob.h"


WiFiClient clientWiFi ;
PubSubClient MQTT(clientWiFi) ;
IPAddress httpServer(192,168,1,122);
IPAddress mqttBrocker(192, 168, 1, 121);

/* === Bibliothèques outils === */
/* ==> PORT CONSOLE ET DEBOGGAGE <== */
void initSerial() {
  Serial.begin(115200);
  Serial.println("Serial série opérationnelle");

  SPI.begin();
}

void logSerial(String msg) {
  Serial.println(msg);
  if (WiFi.status()==WL_CONNECTED)
  {
    logMQTT(msg);
  } ;
  

}

void logMQTT(String msg) {
  
  MQTT.setServer(mqttBrocker, 1883) ;
  int status = MQTT.connect("");
  if (!(status)) {
    logSerial("Impossible de contacter le courtier") ;
    checkWifiLink(true) ;
  }
  char msg_char[255] = {0} ;
  for (int i=0 ; i<msg.length(); i++) {
    msg_char[i]=msg[i];
  }
  status = MQTT.publish("LOG", msg_char) ;
  
  if (!(status)) {
    logSerial("Impossible de publier") ;
    checkWifiLink(true) ;
  }
  MQTT.disconnect() ;
}

/* ==> OUTILS WIFI <== */

void initWifi() {
  logSerial("Démarrage du WiFi");
  
  int status = 0 ;
  while (status != WL_CONNECTED) {
    status = WiFi.begin(mySSID, myPSK) ;
    Serial.print(status);
    Serial.print(WL_CONNECTED);
    delay(1000) ;
  } ;
  
  logSerial("Addresse IP:") ;
  IPAddress IP = WiFi.localIP() ;
  Serial.println(IP) ;
}

boolean wifiLinkAvailable() {
  return WiFi.status()==WL_CONNECTED;
}

/* ==> OUTILS LORA <== */
void loraInit() {
  logSerial("Démarrage de SX1276 sur fréquence " + String(DATA_FREQ));

  LoRa.setPins(SX1276_NSS,
               SX1276_RESET,
               SX1276_DIO0) ;

  if (!LoRa.begin(DATA_FREQ)) {
    logSerial("SX1276 en erreur!");
    while (1);
  }
  else {
    logSerial("SX1276 en fonction!");
  }
}

/* ==> GESTION DES PAQUETS <== */
void simulPacket() {
  RXBuffer[0] = 0 ;
  RXBuffer[1] = 254 ;
  RXBuffer[2] = 0 ;
  RXBuffer[3] = 0 ;
  RXBuffer[4] = 0 ;
  RXBuffer[5] = 0 ;
  RXBuffer[6] = 0 ;
  RXBuffer[7] = 0 ;
  RXBuffer[8] = 0 ;
  RXBuffer[9] = 0 ;
  RXBuffer[10] = 0 ;
  RXBuffer[11] = 0 ;
  RXBuffer[12] = 15 ;
  RXBuffer[13] = 0 ;
  RXBuffer[14] = 0 ;
  RXBuffer[15] = 0 ;
}

void waitForPacket() {
  logSerial("À l'écoute de paquets");

  byte packetSize = 0;
  while (not packetSize) {
    packetSize = LoRa.parsePacket();
  logSerial("Paquet reçu");
  byte index = 0 ;
  while (LoRa.available()) {
    RXBuffer[index] =  LoRa.read();
    Serial.print(RXBuffer[index], DEC);
    Serial.print(",");
    index++ ;
  }
  logSerial(String("Longueur :") + String(index) +
	    String(" [RSSI:") + String(LoRa.packetRssi()) +
	    String(",SNR:") + String(LoRa.packetSnr()) +
	    String(",FRQ ERROR:") + String(LoRa.packetFrequencyError()) +
	    String("]")
	    );

  Serial.println(" * ---- * ");
  }
}

boolean checkPacketValidity() {
  return (
	  (RXBuffer[0] == 0) &&
	  (RXBuffer[1] < DEV_ADDR_MAX) &&
	  (RXBuffer[11] == 0) &&
	  (RXBuffer[12] == FRAME_LENGTH) &&
	  (RXBuffer[13] == 0) &&
	  (RXBuffer[14] == 0)
	  ) ;
}

void logRXBuffer() {
  for (byte index = 0 ; index < FRAME_LENGTH ; index++) {
    logSerial(String(RXBuffer[index]));
  } ;
}

void sendViaMqtt(char buffer[255]) {
  MQTT.setServer(mqttBrocker, 1883) ;
  status = MQTT.connect("");
  if (!(status)) {
    logSerial("Impossible de contacter le courtier") ;
    checkWifiLink(true) ;
  }
  status = MQTT.publish("DRAGINO", buffer) ;
  if (!(status)) {
    logSerial("Impossible de publier") ;
    checkWifiLink(true) ;
  }
  MQTT.disconnect() ;
} ;

void sendViaHTTP(char buffer[255]) {
  if(!client.connected())
  {
  // if the server's disconnected, stop the client:
  Serial.println("disconnected");
  client.stop();
  } ;
  // send HTTP header
 client.println("POST " + PATH_NAME + " HTTP/1.1");
 client.println("Host: " + String(HOST_NAME));
 client.println("Connection: close");
 client.println(); // end HTTP header
 
 // send HTTP body
 client.println(queryString);
 while(client.available())
  {
  // read an incoming byte from the server and print them to serial monitor:
  char c = client.read();
  Serial.print(c);
 }
 if(!client.connected())
  {
  // if the server's disconnected, stop the client:
  Serial.println("disconnected");
  client.stop();
 }
}

void extractDataAndTransmit() {
  String dataString = "";
  for (int i = 1 ; i < 7 ; i++) {
    dataString += String(RXBuffer[i]) + ",";
  }
  float hxValue = 16777216 * RXBuffer[7] +
                  65536 * RXBuffer[8] +
                  256 * RXBuffer[9] +
                  RXBuffer[10] ;
  dataString += String(hxValue, 0) ;
  dataString.replace(" ", "");
  logSerial("Transmission MQTT de " + dataString) ;
  char buffer[255] = {0};
  for (byte i = 0; i < dataString.length(); i++) {
    buffer[i] = (char)dataString[i];
  }

  boolean status = 0 ;

  sendViaMqtt(buffer) ;
  sendViaHttp(buffer) ;
  
}


void processPacket() {
  logSerial("Process Packet") ;
  if (checkPacketValidity()) {
    logSerial("Trame reçue bien formée") ;
    logRXBuffer() ;

    extractDataAndTransmit() ;
  }
  else {
    logSerial("Trame reçue mal formée:" +
              String(RXBuffer[0] == 0)) ;
    logSerial(String(RXBuffer[1] == 0)) ;
    logSerial(String(RXBuffer[11] == 0)) ;
    logSerial(String(RXBuffer[12] == FRAME_LENGTH)) ;
    logSerial(String(RXBuffer[13] == 0)) ;
    logSerial(String(RXBuffer[14] == 0)) ;
  }
}

/* ==== Initialisation ==== */
void setup() {
  initSerial();

  initWifi();
  loraInit();
}

/* ==== Boucle principale ==== */
void loop() {
  byte RXBuffer[256] = {0} ;
  
  logSerial("Starting " + Sketch_Ver) ;
  waitForPacket() ;
  if wifiLinkAvailable() {
      processPacket() ;
    }
  else {
    initWiFi();
  }
  logSerial("Bouclage") ;
}

#!/usr/bin/sh

adduser --shell=/bin/false --no-create-home --disabled-password ruche
mkdir /opt/ruche
chown -R ruche:ruche /opt/ruche
cp src-python/serveur.py /opt/ruche
cp -r src-python/static /opt/ruche
cp -r src-python/templates /opt/ruche
cp systemd/ruche.service /etc/systemd/system

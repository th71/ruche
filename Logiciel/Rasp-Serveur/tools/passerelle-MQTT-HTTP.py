import sys
from paho.mqtt import subscribe
from datetime import datetime
import logging
from urllib import request as httpclient
from urllib import parse
import requests
from daemonize import Daemonize
import json

logging.basicConfig(level=logging.INFO)

try:
    cmd = sys.argv[1]
except:
    cmd=""

print("COMMANDE:",cmd)

def getMqttMessage():
    return subscribe.simple("DRAGINO", hostname="tigwened.hopto.org", port=61883)

def cookMessage(message):
    data=message.payload.decode("UTF-8").split(",")
    data=[int(val) for val in data]
    timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    data.insert(0, timestamp)
    data.insert(1, "mqtt")
    dico_data = dict(zip(['date', 'source', 'id', 'press', 'temp_ext', 'humid', 'temp_int1', 'temp_int2', 'poids'],data))
    
    dico_data['press']+=960
    dico_data['temp_ext']-=128
    dico_data['temp_int1']-=128
    dico_data['temp_int2']-=128
    logging.info("incoming data:"+str(dico_data))
    return dico_data
    
def capture(save=True):
    while True:
        message = getMqttMessage()
        dico_data = cookMessage(message)
        requests.post("http://localhost:5000/data", json=dico_data)

if cmd=="capture":
    capture(save=False)

if cmd=="store":
    capture()

if cmd=="daemon":
    daemon = Daemonize(app="passerelle-MQTT-HTTP", pid="/tmp/mqtt-http.pid", action=capture)
    daemon.start()
    


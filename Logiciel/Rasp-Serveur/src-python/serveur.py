#/usr/bin/env python3
import sys
import sqlite3
import urllib
from datetime import datetime
from flask import Flask, request, jsonify, Response, render_template, send_file
from datetime import datetime
import numpy
import pandas
import matplotlib
matplotlib.use("Agg")
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import logging
from io import BytesIO
from sqlalchemy import create_engine
import matplotlib.dates as mdates
from daemonize import Daemonize
from time import sleep

logging.basicConfig(level=logging.INFO)

fields=[ "date", "source", "id", "temp_ext", "press" , "humid", "temp_int1", "temp_int2", "poids"]

def build_table(force=False):
    base=sqlite3.connect("base.sqlite")
    try:
        base.execute("CREATE TABLE donnees (date text, source text, id int, temp_ext int, pression int, humid int, temp_int1 int, temp_int2 int , poids int)")
    except sqlite3.OperationalError:
        if not(force):
            print("Base existante, confirmer la re-création:[o/N]")
            if input()=="o":
                base.execute("DROP TABLE donnees")
                base.execute("CREATE TABLE donnees (date text, source text, id int, temp_ext int, pression int, humid int, temp_int1 int, temp_int2 int , poids int)")
            else:
                print("Opération annulée")
        else:
            pass
    finally:
        print("Base initialisée")

    base.commit()
    base.close()

# ##################### #
### Application Flask ###
# ##################### #

application = Flask(__name__)

application.config['DEBUG'] = True

@application.route("/")
def indexroute():
    return render_template("index.html.tpl")

@application.route("/table")
def tableroute():
    with sqlite3.connect("base.sqlite") as base:
        query = base.execute("SELECT * FROM donnees")
        data = [result for result in query.fetchall()]
    data=[[numpy.datetime64(a[0]),*a[1:]] for a in data]
    df = pandas.DataFrame(data, columns=['date', 'source', 'id', 'temp_ext', 'press',  'humid', 'temp_int1', 'temp_int2', 'poids'])
    htmltable = df.to_html()
    return render_template("table.html.tpl", table=htmltable)

@application.route("/csv")
def csvroute():
    with sqlite3.connect("base.sqlite") as base:
        query = base.execute("SELECT * FROM donnees")
        data = [result for result in query.fetchall()]
    data=[[numpy.datetime64(a[0]),*a[1:]] for a in data]
    df = pandas.DataFrame(data, columns=['date', 'source' ,'id', 'temp_ext', 'press',  'humid', 'temp_int1', 'temp_int2', 'poids'])
    df = df.drop(columns=['source', 'id'])
    df = df.set_index("date")
    csvtable = df.to_csv()
    print(csvtable)
    fichier = BytesIO()
    for line in csvtable:
        fichier.write(line.encode("utf-8"))
    fichier.flush()
    fichier.seek(0)
    return send_file(fichier, mimetype='Content-Type: text/csv ; charset="utf-8"', attachment_filename='donnees.csv', as_attachment=True)

@application.route("/data", methods=["POST"])
def dataroute():
    print(request.data)
    data = request.get_json()
    logging.info(f"Flask POST on /data:{str(data)}")

    datasetok = True
    for key in data.keys():
        if key not in fields:
            logging.info(f"Clef {key} inconnue")
            datasetok = False
    if datasetok:
        logging.info("JSON OK")

        try:
            base=sqlite3.connect("base.sqlite")
            base.execute("INSERT INTO donnees VALUES (?,?,?,?,?,?,?,?,?)", (data["date"], f"http via {data['source']}", data["id"], data["temp_ext"],
                                                                            data["press"], data["humid"],
                                                                            data["temp_int1"], data["temp_int2"],
                                                                            data["poids"]))
            base.commit()
            logging.info("Database injection ok")
            base.close()
        except Exception as e:
            logging.error(f"Error when storing in database:{e}")

    return "OK"

base_uri = "file:./base.sqlite?mode=ro&uri=true"

@application.route("/trace")
def traceview():
    return render_template("trace.html")

def viewTemperatures(dataFrame):
    #return dataFrame.tEXT
    return dataFrame[['tEXT', 'tINT1', 'tINT2']]

def viewMoisture(dataFrame):
    return dataFrame.hEXT

def viewPressure(dataFrame):
    return dataFrame.pEXT

def viewWeight(dataFrame):
    return dataFrame.poids

def getDataFromDatabase():
    base=sqlite3.connect("base.sqlite")
    cur = base.execute("SELECT * FROM donnees")
    results = [data for data in cur.fetchall()]
    base.close()
    return results

@application.route("/json")
def jsonroute():
    jsondata=[]
    for data in getDataFromDatabase():
        if data[0]!="----":
            datazip = zip(['date', 'source', 'id', 'temp_ext', 'press',  'humid', 'temp_int1', 'temp_int2', 'poids'],data)
            dico_data={key:val for key,val in datazip}
            jsondata.append(dico_data)
    return jsonify(jsondata)

@application.route("/graph/<kind>")
def graphview(kind):
    print("=====>", kind)
    data = getDataFromDatabase()
    dataFrame = pandas.DataFrame(data)
    dataFrame.columns=["date", "source", "id", "tEXT", "pEXT", "hEXT", "tINT1", "tINT2", "poids"]
    dataDate = dataFrame['date']
    dataFrame.drop(columns=['date'])

    print(dataDate)
    dataDate = pandas.to_datetime(dataDate)
    print(dataDate)
    dataFrame=dataFrame.set_index(dataDate)
    dataFrame=dataFrame.loc[dataFrame.loc[:,'id']==0]

    fig = Figure()
    subplot = fig.add_subplot(1,1,1)
    print(dir(subplot))
    xaxis = subplot.xaxis
    xaxis.set_major_formatter(mdates.DateFormatter("%Y/%m/%d %H:%M"))
    xaxis.set_major_locator(mdates.AutoDateLocator())

    if kind=="temperatures":
        data = viewTemperatures(dataFrame)
        subplot.plot(data)
        subplot.set_title("Graphique des températures")
        subplot.set_xlabel("Date")
        subplot.set_ylabel("°C")
        subplot.legend(["Température Extérieure", "Température Intérieure", "Température Objet"])

    if kind=="moisture":
        data = viewMoisture(dataFrame)
        subplot.plot(data) 
        subplot.set_title("Graphique de l'humidité extérieure")
        subplot.set_xlabel("Date")
        subplot.set_ylabel("%")
        
    if kind=="pressure":
        data = viewPressure(dataFrame)
        subplot.plot(data) 
        subplot.set_title("Graphique de la pression atmosphérique")
        subplot.set_xlabel("Date")
        subplot.set_ylabel("hPa")
        
    if kind=="weight":
        data = viewWeight(dataFrame)
        subplot.plot(data)
        subplot.set_title("Graphique du poids")
        subplot.set_xlabel("Date")
        subplot.set_ylabel("kg")
        
    fig.autofmt_xdate()
    outputBuffer = BytesIO()
    FigureCanvas(fig).print_jpg(outputBuffer)
    
    print("Return", kind, outputBuffer)
    
    return Response(outputBuffer.getvalue(), mimetype='image/jpg')

def capture(save):
    application.run()

def getMode():
    try:
        cmd = ",".join(sys.argv)
    except:
        cmd = ""

    return cmd

#Programme principal

cmd = getMode()
logging.info("Starting with command:"+cmd)

if  "initbase" in cmd:
    build_table()

if "capture" in cmd:
    capture(save=False)

if "store" in cmd:
    capture(save=True)

if "view" in cmd:
    base = sqlite3.connect("base.sqlite")
    cur = base.execute("SELECT * FROM donnees")
    for entry in cur.fetchall():
        print(entry)
    

if "run" in cmd:
    print(application.config)
    application.run()

